-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2023 at 12:03 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `soup_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `fk_id_user` int(11) NOT NULL,
  `fk_id_product` int(11) NOT NULL,
  `schedule` date NOT NULL,
  `isPaid` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `isActivated` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `isActivated` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `NAME`, `DESCRIPTION`, `image`, `isActivated`) VALUES
(1, 'Asian', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 'uploads/e27096ff-7c33-47a6-8a79-5e9648803c2d.png', 1),
(2, 'Western', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 'uploads/2f6a9ca1-0be6-44c2-afe3-a92f4e3cacb7.png', 1),
(3, 'Cookies', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 'uploads/1fcfd587-9b24-473c-a05c-b5cb4bedb68a.png', 1),
(4, 'Indonesia Food', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 'uploads/d971eea9-c0a6-4525-b331-0ff86bf2f707.jpg', 1),
(5, 'Hot Drink', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 'uploads/aedb5c51-31ee-4afd-9407-082d10dc06a2.png', 1),
(6, 'Cold Drink', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 'uploads/6c8bacf4-7905-4dc7-af34-94faa696dfc0.png', 1),
(7, 'Junk Food', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 'uploads/4571ec5f-320e-484e-9db1-5fa0089adfe7.png', 1),
(8, 'Desert', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 'uploads/4ebb9ef6-f4ac-402a-83b8-acef2be91e88.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `fk_id_user` int(11) NOT NULL,
  `fk_id_payment_method` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `isActivated` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` int(11) NOT NULL,
  `fk_id_invoice` int(11) NOT NULL,
  `fk_id_product` int(11) NOT NULL,
  `cost_per_product` int(11) NOT NULL,
  `schedule` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `isActivated` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `isActivated` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_method`
--

INSERT INTO `payment_method` (`id`, `NAME`, `image`, `isActivated`) VALUES
(1, 'Gopay', 'uploads/3811123f-3bb4-4df2-9154-403169278997.png', 1),
(2, 'OVO', 'uploads/b185d674-bf0b-45cc-a743-2aa8f28079c0.png', 1),
(3, 'DANA', 'uploads/7a843a15-3961-40a6-a4e7-14e1a99883a6.png', 1),
(5, 'Mandiri', 'uploads/07b1ce53-e238-4eca-bed7-29d062d84d56.png', 1),
(6, 'BCA', 'uploads/bd134de0-d418-4056-8e55-819d5eb476e3.png', 1),
(7, 'BNI', 'uploads/608d221b-c712-4b78-8d90-cc9d19825236.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `image` text NOT NULL,
  `fk_id_category` int(11) NOT NULL,
  `isActivated` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `NAME`, `DESCRIPTION`, `price`, `image`, `fk_id_category`, `isActivated`) VALUES
(1, 'Sate Padang', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 300000, 'uploads/98df976e-0ded-4af1-987f-08b6d67aa55b.png', 1, 1),
(2, 'Sushi Salmon with Mentai', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 200000, 'uploads/8f2cad6a-f383-4b09-9cbe-81360a5eb112.png', 1, 1),
(4, 'Takoyaki Octopus', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 150000, 'uploads/f1256275-9eb0-4adb-9473-0b4ccf267852.png', 1, 1),
(7, 'Rendang Sapi', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 200000, 'uploads/64c59100-6a04-4a34-bae7-070e2f3f9093.png', 1, 1),
(8, 'Soto Banjar Limau Kuit', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 150000, 'uploads/65f24da2-8c52-407c-be36-4c0446807f04.png', 1, 1),
(9, 'Ichiraku Ramen', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 300000, 'uploads/f08c5666-daa5-472f-80e2-9fe6483b5e1b.png', 1, 1),
(10, 'Tom Yum Thailand', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 450000, 'uploads/aab55935-b664-48a3-9761-e180f01c9894.png', 1, 1),
(11, 'Jajangmeyon', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 250000, 'uploads/e9de976a-faf8-4821-a35f-8aac2c3ff886.png', 1, 1),
(12, 'Italian Spaghetti Bolognese', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 450000, 'uploads/00956b30-cd7d-4838-9fec-a08d85cd218d.png', 2, 1),
(13, 'Chocholate Cookies', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 200000, 'uploads/b88305e3-4cb2-4f4b-9a53-b1c790576637.png', 3, 1),
(14, 'Gudeg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 200000, 'uploads/c9329e81-a018-4bb7-b461-4f14d5b270ee.jpg', 4, 1),
(15, 'Caramel Frapucchino', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 300000, 'uploads/2abebd42-c91a-46ba-9a2a-08560723b933.png', 5, 1),
(16, 'Strawberry Float', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 150000, 'uploads/129db853-92bd-4acc-82f0-d3ffd3cf30d8.png', 6, 1),
(17, 'Pizza', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 400000, 'uploads/16362827-202a-44be-97ad-154f63dcfbe2.png', 7, 1),
(18, 'Green Tea Cheesecake', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 400000, 'uploads/2ab21ab7-2619-456a-8a1b-34e69454da1e.png', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `IsConfirmed` tinyint(1) NOT NULL DEFAULT 0,
  `token_password` varchar(255) DEFAULT NULL,
  `isActivated` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `NAME`, `email`, `PASSWORD`, `role`, `token`, `IsConfirmed`, `token_password`, `isActivated`) VALUES
(1, 'Renaldi', 'renren@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'user', NULL, 1, NULL, 1),
(2, 'Setiawan Dwi Novantoro', 'wawan@gmail.com', 'aa6ae8c005b9048b03f6059224c858650d9e52d5', 'user', NULL, 1, NULL, 1),
(3, 'Duri', 'user@gmail.com', '12dea96fec20593566ab75692c9949596833adc9', 'user', NULL, 1, NULL, 1),
(4, 'ADMIN', 'admin@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'admin', NULL, 1, NULL, 1),
(5, 'Aldi', 'aldi@gmail.com', '7C4A8D09CA3762AF61E59520943DC26494F8941B', 'user', NULL, 1, NULL, 1),
(6, 'UserDummy', 'userdummy@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'user', NULL, 1, NULL, 1),
(7, 'UserDummyRegister', 'userdummyregister@gmail.com', '7C222FB2927D828AF22F592134E8932480637C0D', 'user', NULL, 1, NULL, 1),
(8, 'Dummy', 'userdummy222@gmail.com', '7C222FB2927D828AF22F592134E8932480637C0D', 'user', NULL, 1, NULL, 1),
(9, 'DummyUserName', 'dummydummyemail@gmail.com', '7C222FB2927D828AF22F592134E8932480637C0D', 'user', NULL, 1, NULL, 1),
(10, 'RenaldiRenaldi', 'renaldirenaldi@gmail.com', '7C222FB2927D828AF22F592134E8932480637C0D', 'user', NULL, 1, NULL, 1),
(12, 'Andi', 'andi@gmail.com', 'A7D579BA76398070EAE654C30FF153A4C273272A', 'user', NULL, 1, NULL, 1),
(13, 'budi', 'bud@gmail.com', '7C222FB2927D828AF22F592134E8932480637C0D', 'user', '491bcb10-d5e2-4c4a-bfe2-96d766fd01c4', 1, NULL, 1),
(14, 'bedu', 'bedu@gmail.com', 'A7D579BA76398070EAE654C30FF153A4C273272A', 'user', '5c7f4466-fca6-4b19-8a18-719df53ec4ab', 1, NULL, 1),
(21, 'Zenia', 'zz352512@gmail.com', '7C222FB2927D828AF22F592134E8932480637C0D', 'user', '5cbe6aca-24a6-4cca-a939-fb6175db94bc', 1, NULL, 1),
(26, 'Renaldi', 'razalerkun@gmail.com', 'A7D579BA76398070EAE654C30FF153A4C273272A', 'user', 'b3f631df-0155-4683-a8e5-76bfafaeec60', 1, '0df0979a-497b-4358-a9da-2a77a0b5dab9', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_user` (`fk_id_user`),
  ADD KEY `fk_id_product` (`fk_id_product`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_user` (`fk_id_user`),
  ADD KEY `fk_id_payment_method` (`fk_id_payment_method`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_invoice` (`fk_id_invoice`),
  ADD KEY `fk_id_product` (`fk_id_product`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_category` (`fk_id_category`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`fk_id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`fk_id_product`) REFERENCES `product` (`id`);

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`fk_id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `invoice_ibfk_2` FOREIGN KEY (`fk_id_payment_method`) REFERENCES `payment_method` (`id`);

--
-- Constraints for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD CONSTRAINT `invoice_details_ibfk_1` FOREIGN KEY (`fk_id_invoice`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `invoice_details_ibfk_2` FOREIGN KEY (`fk_id_product`) REFERENCES `product` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`fk_id_category`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
